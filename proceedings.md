---
layout: post
title: Conference Proceedings
permalink: /proceedings/
isStaticPost: true
image: 2017-ad.jpg
---
<h4>Day 1</h4>
<ul>
{% for talk in site.data.pretalx.schedule.one.talks %}
  <li>
    {{ talk.title }} 
    &mdash;
    {% for person in talk.persons %}
      <em>{{person.public_name}}, </em>
    {% endfor %}   
    {% if talk.video != null %} &mdash;
    <a href="{{ talk.video }}"> Video</a>
    {% endif %}
    {% if talk.slide != null %}&mdash;
    <a href="{{ talk.slide }}"> Slides</a>
    {% endif %}
  </li>
{% endfor %}
</ul>

<h4>Day 2</h4>
<ul>
{% for talk in site.data.pretalx.schedule.two.talks %}
  <li>
    {{ talk.title }} 
    &mdash;
    {% for person in talk.persons %}
      <em>{{person.public_name}}, </em>
    {% endfor %}   
    {% if talk.video != null  %} &mdash;
    <a href="{{ talk.video }}"> Video</a>
    {% endif %}
    {% if talk.slide != null %}&mdash;
    <a href="{{ talk.slide }}"> Slides</a>
    {% endif %}
  </li>
{% endfor %}
</ul>


<h4>Day 3</h4>
<ul>
{% for talk in site.data.pretalx.schedule.three.talks %}
  <li>
    {{ talk.title }} 
    &mdash;
    {% for person in talk.persons %}
      <em>{{person.public_name}}, </em>
    {% endfor %}   
    {% if talk.video != null %} &mdash;
    <a href="{{ talk.video }}"> Video</a>
    {% endif %}
    {% if talk.slide != null %}&mdash;
    <a href="{{ talk.slide }}"> Slides</a>
    {% endif %}
  </li>
{% endfor %}
</ul>
